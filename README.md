# NGSMarket #

This is a data & statistics collection application created by me for use on Phantasy Star Online 2 New Genesis's marketplace. This application
uses a client - server model. 

Worker clients will be assigned tasks from the server to lookup an item on the marketplace. This process is fully automated but requires an Arduino with HID-Keyboard support (Arduino Leonardo or better) to bypass GameGuard's hooks from preventing KeyEvents and MouseEvents. Data is extracted from captured images using Optical Character Recognition (OCR) through the Tesseract and Tess4J libraries. After conversion to data, it is uploaded to the Server client where it is stored in a SQLite database.

Worker clients come in three different operating versions. Converter, Capture, and Superworker. Converter and Capture workers are intended for use on systems that need to maintain their processing power or are weaker. These two workers do not feature any multithreading in their programming.  Converters will only convert captured images into data and send it to the server while Capture workers will only capture images and store it inside of the unprocessedimages folder. Superworkers can do both the functions of Converter and Capture workers and are multithreaded. Both Capture and Superworker clients require the PC to not be in use while it is running. Converter clients may run in the background and the user can continue using their PC like normal.

# Instructions #

Be sure to set the server-ip property inside of configs.properties to the PC running the Server client for your Worker client before launching or else the program will not work. If the server PC is on a different network, then be sure to port forward TCP/UDP port 7555 on both systems to ensure that connectivity can be established. You can also run the server locally on your PC alongside a worker client if you intend to use it for a small community group.

Each PC that runs a Capture or Superworker client must be flashed with the program inside of the arduino folder. Your device must also display "Arduino Leonardo" (Regardless of whether or not it is actually an Arduino Leonardo) within the device manager for the program to function properly. You can do this by following the guide below. Converter clients do not require an Arduino at all and also does not require PSO2NGS to be running to operate.

https://www.avoiderrors.com/rename-devices-device-manager/

Ensure that you open up the marketplace search screen up in PSO2NGS and keep focus on the game. The window sizes for the search and results screen should be at approximately the default setting. If you resized the window size of the marketplace search and results screen drastically, then please see the unprocesseddatacopies for the approximate size and placement the window needs to be changed to. Or you can simply reset your windows settings altogether.

# Implemented Features #
- Item lookup
- Support for different screen resolutions
- Arduino automated lookup and capture
- OCR data conversion
- SQLite database storage

# TODO #
- Seperation of Worker class into different class files.
- SMS alerts for either stock or price drops.
- Website that can access the main database.
- Server maintains clients and automatically distributes item lookup requests equally amongst them.
- Checks to ensure that queued items were actually captured and sent to the server.

